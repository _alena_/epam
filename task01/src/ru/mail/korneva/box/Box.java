package ru.mail.korneva.box;
import ru.mail.korneva.figure.Figure;
import ru.mail.korneva.film.IFilm;
import ru.mail.korneva.figure.Circle;
/**
 * created by korneva on 25.04.2019
 */
public class Box {

    private final int SIZE;
    private Figure[]figures;
    public Box(int SIZE){
      this.SIZE = SIZE;
      figures = new Figure [this.SIZE];
    }
    /**
     * фигура помещается в коробку
     * @param index
     * @param figure
     * @return
     */
    public Figure setFigure(int index, Figure figure) {
        Figure oldFigure = figures[index];
        figures[index] = figure;
        return oldFigure;

    }
    /**
     * возвращаем фигуру по ее порядковому номеру(фигура остается в коробке)
     * @param index
     * @return
     */
    public Figure getFigure(int index){
        return figures[index];
    }


    // figure объект, в который записали объект из массива figures
    /**
     * извлечение фигуры по номеру(удаляется из коробки)
      */
    public Figure removeFigure(int index){
       Figure figure = figures[index];
       figures[index]=null;//удаление фигуры из коробки
       return figure;
    }
    /**
     * считаем количество фигур в коробке
     * @return
     */
    public int getCount(){
        int count = 0;
        //проверка на количество до 20 и на отрицательное число
        if (SIZE >= 0 && SIZE < 20) {
            for (int i = 0; i < SIZE; i++) {
                if (figures[i] != null) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     *найти по образцу
     * @param figure
     * @return
     */
    public boolean findByFigure(Figure figure){
        if(figure == null){
            return false;
    }
        //проверка, лежит ли уже в коробке такая фигура (сравнение по ссылке на расположение фигуры)
        for(Figure figure1 : figures){
            if(figure.equals(figure1)){
                return true;
            }
        }
        return false;
}
    /**
     * посчитать все круги
     * @return
     */
    public int getCountCircle() {
        int count=0;
        for (int i=0; i<SIZE; i++) {
            if (figures[i] !=null && figures[i] instanceof Circle) {
                count++;
            }
        }
        return count;
    }
    /**
     * достать все круги
     * @return
     */
    public Box getAllCircle() {
        Box circleBox = new Box(getCountCircle());
        int index = 0;
        for (int i = 0; i < SIZE; i++) {
            if (figures[i] != null && figures[i] instanceof Circle) {
                circleBox.setFigure(index, figures[i]);
                index++;
            }
        }
        return circleBox;
    }
        /**
         * посчитать все пленочные фигуры
         * @return
         */
        public int getCountFilm () {
            int count = 0;
            for (int i = 0; i < SIZE; i++) {
                if (figures[i] != null && figures[i] instanceof IFilm) {
                    count++;
                }
            }
            return count;
        }
        /**
         * достать все пленочные фигуры
         * @return
         */
        public Box getAllFilm () {
            Box filmBox = new Box(getCountFilm());
            int index = 0;
            for (int i = 0; i < SIZE; i++) {
                if (figures[i] != null && figures[i] instanceof IFilm) {
                    filmBox.setFigure(index, figures[i]);
                    index++;
                }
            }
            return filmBox;
        }

    /**
     * посчитать суммарную площадь
     * @return
     */
    public double getSumArea() {
            double sum=0;
            for (Figure figure: figures) {
                sum += figure.getArea();
            }
            return sum;
        }
    /**
     * посчитать суммарный периметр
      */
    public double getSumPerimeter() {
        double sum=0;
        for (Figure figure: figures) {
            sum += figure.getPerimeter();
        }
        return sum;
    }

}