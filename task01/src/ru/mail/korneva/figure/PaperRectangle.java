package ru.mail.korneva.figure;

import ru.mail.korneva.paper.IPaper;

import java.util.Objects;

public class PaperRectangle extends Rectangle implements IPaper {
    private Painted painted = new Painted();

    public PaperRectangle (double sideA, double sideB){
    super(sideA, sideB);
    }
public PaperRectangle(Figure paperFigure){
super((Figure)paperFigure);
painted = paperFigure.getPainted();
    }
    @Override
   public Color getColor(){
        return painted.getColor();
   }
    public void setColor(Color color){
        Painted.setColor(color);
    }
   @Override
    public Painted getPainted(){
        return painted;
   }
    public String toString(){
        return super.toString() + " painted = " + painted.getColor();
    }
    @Override
    public boolean equals(Objects o){
        if (this == 0) return true;
        if (!super.equals(o)) return false;
        PaperRectangle paperRectangle = (PaperRectangle) o;
        return Objects.equals(paperRectangle.painted, painted);
    }
    @Override
    public int hashCode(){
        return Objects.hash(painted);
    }
}
