package ru.mail.korneva.figure;

import java.util.Objects;

import static java.lang.Math.PI;

/**
 * Created by korneva on 22.04.2019
 */
public abstract class Circle extends Figure{
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }
    public Circle(Figure figure){
        this.radius = figure.getMinSize()/2;
    }

    /**
     *
     * @return radius
     */
    @Override
    public double getMinSize(){
        return radius;
    }
    /**
     *
     * @param radius
     */
     public double setRadius (double radius){
            this.radius = radius;
            return radius;
        }
    /**
     * @return Math.PI * radius * radius
     */
    @Override
    public double getArea(){
        return PI*radius*radius;
    }
    /**
     * @return Math.PI * radius * 2
     */
    @Override
    public double getPerimeter(){
        return PI*radius;
    }
    /**
     * @return super.toString() + "radius =" + radius
     */
    @Override
    public String toString(){
        return super.toString() + " :radius= " + radius;
    }
@Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(!super.equals(o)) return false;
        Circle circle =(Circle) o;
        return Double.compare(circle.radius, radius) == 0;
        //circle.radius параметр, который принимаем для сравнения
    //radius имеющийся радиус

}
    @Override
    public int hashCode(){
return Objects.hash(radius);
    }
}
