package ru.mail.korneva.figure;

/**
 * Created by korneva on 22.04.2019
 */
public abstract class Figure {
    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract double getMinSize();

    @Override
    public String toString(){
        return
                this.getClass().getSimpleName();
    }
    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        return getClass() == o.getClass();
    }
}
