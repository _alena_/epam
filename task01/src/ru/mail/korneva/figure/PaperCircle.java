package ru.mail.korneva.figure;

import ru.mail.korneva.paper.IPaper;

import java.util.Objects;

public class PaperCircle extends Circle implements IPaper {
    private Painted painted = new Painted();

    public PaperCircle(double radius) {
        super(radius);
    }

    public PaperCircle(IPaper paperFigure) {
        super((Figure) paperFigure);
        painted = paperFigure.getPainted();
    }
    //переопределение абстрактных методов интрерфейса paper
    @Override
    public Color getColor(){
        //метод внутреннего класса Color интерфейса
        return painted.getColor();
    }
    @Override
    public void setColor(Color color){
        Painted.setColor(color);
    }
    @Override
    public Painted getPainted(){
        return painted;
    }
    @Override
    public String toString(){
        return super.toString() + " painted = " + painted.getColor();
    }

@Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(o == null || getClass()!= o.getClass()) return false;
        if(!super.equals(0)) return false;
        PaperCircle paperCircle = (PaperCircle) o;
        return Objects.equals(paperCircle.painted, painted);

}
    @Override
    public int hashCode(){
        return Objects.hash(painted);
    }
}
