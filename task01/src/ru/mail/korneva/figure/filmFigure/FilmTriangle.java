package ru.mail.korneva.figure;

import ru.mail.korneva.film.IFilm;

public class FilmTriangle extends Triangle implements IFilm {
    public FilmTriangle(double triangleSide){
     super(triangleSide);
    }
    public FilmTriangle (Figure filmFigure){
    super((Figure)filmFigure);
    }
}
