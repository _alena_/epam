package ru.mail.korneva.figure;

import ru.mail.korneva.film.IFilm;
import ru.mail.korneva.box.Box;

public class FilmCircle extends Circle implements IFilm {
    public FilmCircle(double radius){
        super(radius);
    }
    public FilmCircle(Figure filmFigure){

        super((Figure)filmFigure);
    }

}
