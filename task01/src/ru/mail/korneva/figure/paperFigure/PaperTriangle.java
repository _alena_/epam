package ru.mail.korneva.figure;

import ru.mail.korneva.paper.IPaper;

import java.util.Objects;

public class PaperTriangle extends Triangle implements IPaper {
    private Painted painted = new Painted();

    public PaperTriangle(double triangleSide){
        super(triangleSide);
    }
    public PaperTriangle(Figure paperFigure){
        super((Figure)paperFigure);
        painted = paperFigure.getPainted();
    }
    @Override
    public Color getColor(){
        //метод внутреннего класса Color интерфейса
        return painted.getColor();
    }
    @Override
    public void setColor(Color color){
        painted.setColor(color);
    }
    @Override
    public Painted getPainted(){
        return painted;
    }
    @Override
    public String toString(){
        return super.toString() + " painted = " + painted.getColor();
    }
    @Override
    public boolean equals(Objects o){
    if (this == o) return true;
    if (! super.equals(0)) return false;
    PaperTriangle paperTriangle = (PaperTriangle)o;
    return Objects.equals(paperTriangle.painted, painted);
}
@Override
    public int hashCode(){
        return Objects.hash(painted);
}
}
