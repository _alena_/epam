package ru.mail.korneva.figure;
import java.util.Objects;
/**
 * created by korneva on 22.04.2019
 */
public abstract class Rectangle extends Figure {
    private double sideA;
    private double sideB;
    public Rectangle(double sideA, double sideB){
        this.sideA = sideA;
        this.sideB = sideB;
    }
    public Rectangle(Figure figure){
        this.sideA=figure.getMinSize()/2;
        this.sideB=sideA;
    }
    /**
     *
     * @return Math.min(sideA,sideB)
     */
    public double getMinSize(){
        return Math.min(sideA,sideB);
    }
    /**
     * @return sideA
     */
    public double getSideA(){
        return sideA;
    }
    /**
     * @param sideA
     */
    public double setSideA(){
        this.sideA = sideA;
    }
    /**
     * @return sideB
     */
    public double getSideB(){
        return sideB;
    }
    /**
     * @param sideB
     */
    public double setRSideB(){
        this.sideB = sideB;
    }
    /**
     * @return sideA * sideB
     */
    @Override
    public double getArea() {
        return sideA*sideB;
    }
    /**
     * @return (sideA + sideB)*2
     */
    @Override
    public double getPerimeter(){
        return (sideA+sideB)*2;
    }
    /**
     * @return super.toString()+"sideA= "+ sideA +"sideB= "+ sideB
     */
    @Override
    public String toString(){
        return super.toString() + " :sideA = " + sideA;
        return super.toString() + " :sideB = " + sideB;
    }
    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!super.equals(o)) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare (rectangle.sideA, sideA) == 0
        && Double.compare (rectangle.sideB, sideB)  == 0;
    }
    @Override
    public int hashCode(){
        return Objects.hash(sideA, sideB);

    }
}
