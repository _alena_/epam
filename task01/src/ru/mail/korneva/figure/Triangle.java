package ru.mail.korneva.figure;

import java.util.Objects;

/**
 * Created bu korneva on 22.04.2019
 * Equilateral triangle with side "triangleSide"
 */
public abstract class Triangle extends Figure {
    private double triangleSide;

    private Triangle(double triangleSide) {
        this.triangleSide = triangleSide;
    }

    private Triangle(Figure figure) {

        this.triangleSide = figure.getMinSize() / 3;
    }

    /**
     * @return triangleSide
     */
    public double getTriangleleSide() {
        return triangleSide;
    }

    /**
     * @return triangleSide
     */
    @Override
    public double getMinSize() {
        return triangleSide;
    }

    /**
     * @return to set triangleSide
     */


    /**
     * @return triangleSide*triangleSide*Math.sqrt(3)/4
     */
    @Override
    public double getArea() {
        return (double) triangleSide * triangleSide * Math.sqrt(3) / 4;
    }

    /**
     * @return triangleSide*3
     */
    @Override
    public double getPerimeter() {
        return triangleSide * 3;
    }

    /**
     * @return super.toString + " :triangleSide = "+ triangleSide;
     */
    @Override
    public String toString() {
        return super.toString() + " :triangleSide = " + triangleSide;

    }
    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(!super.equals(o)) return false;
        Triangle triangle =(Triangle) o;
        return Double.compare(triangle.triangleSide, triangleSide) == 0;


    }
    @Override
    public int hashCode(){
        return Objects.hash(triangleSide);
    }
}
