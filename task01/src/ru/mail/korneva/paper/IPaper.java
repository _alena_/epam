package ru.mail.korneva.paper;

public interface IPaper {
    enum Color {
        RED, YELLOW, GREEN;
    }
    class Painted {
        private Color color;//частная переменная колор, которая принадлежит к энуму колор
        public Color getColor(){
            return color;
        }
        public void setColor(Color color){
            if (this.color == null){
                this.color = color;
            }
        }
    }
    //эти методы нужно определить в каждом из классов они абстрактные
    Color getColor();
    void setColor(Color color);
    Painted getPainted();

}
